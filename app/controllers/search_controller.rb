class SearchController < ApplicationController
  def search
    @results = Post.search(params[:key])
    render 'index'
  end
end
