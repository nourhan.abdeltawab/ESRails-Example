class Post < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  settings index: { number_of_shards: 1 }

  def as_indexed_json(options = {})
    self.as_json({only: [:title, :text]})
  end
end
