Rails.application.routes.draw do
  resources :posts
  resource :search, only: :index do
    collection do
      post 'search', controller: :search
    end
  end

  root 'search#index'
end
